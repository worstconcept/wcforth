class_name WCForthWordBuiltin
extends WCForthWord

@export var name : String
var _func: Callable

func _remap(iname: StringName)->StringName:
	const _lst := {
		&"colon": &":",
		&"icolon": &"::",
		&"semi": &";",
		&"paren": &"(",
		&"quot": &"\"",
		&"bracket": &"[",
		&"comma": &",",

		&"plus": &"+",
		&"minus": &"-",
		&"multiply": &"*",
		&"divide": &"/",
		&"call": &"!!",
		&"read": &"?",
		&"write": &"!",
		&"eq": &"=",
		&"lt": &"<",
		&"lshift": &"<<",
		&"rshift": &">>",
		&"mod": &"%",

		&"nodepath": &"^\"",
		&"stringname": &"#\"",
	}
	if _lst.has(iname):
		return _lst[iname]
	return iname

static func install(VM: WCForth, tgt: Script)->void:
	for method: Dictionary in tgt.get_script_method_list():
		#_install(VM, word, word)
		var mn := method.name as String
		if mn.begins_with("_exec_"):
			mn = mn.trim_prefix("_exec_")
		elif mn.begins_with("_execi_"):
			mn = mn.trim_prefix("_execi_")
		else:
			continue
		assert(&"WCForth" == method.args[0].class_name or ClassDB.is_parent_class(&"WCForth", method.args[0].class_name as StringName))
		_install(VM, tgt, mn)
static func _install(VM: WCForth, tgt: Script, fname: StringName)->void:
	@warning_ignore("unsafe_method_access")
	var res : WCForthWordBuiltin = tgt.new()
	res.name = fname
	if res.has_method("_execi_"+fname):
		res.immediate = true
	else:
		assert(res.has_method("_exec_"+fname))
	VM.dictionary[res._remap(res.name)] = res

func exec(VM: WCForth)->void:
	if not _func.is_valid():
		var _funcname := "_exec"
		if immediate:
			_funcname += "i"
		_funcname += "_"+name
		assert(has_method(_funcname),"No builtin impl for "+_funcname)
		_func = get(_funcname)
	await _func.call(VM)
	if VM.builtin_await:
		await VM.builtin_await

# ---

func _exec_colon(VM: WCForth)->WCForthWordCompiled:
	var wname := VM.read_word()
	var word := await VM._compile_inplace()
	VM.dictionary[wname] = word
	return word
func _exec_icolon(VM: WCForth)->void:
	var w := await _exec_colon(VM)
	w.immediate = true
func _execi_semi(VM: WCForth)->void:
	VM.breaks = 2

func _execi_paren(VM:WCForth)->void:
	VM.read_word(")")
func _execi_quot(VM:WCForth)->void:
	var st := VM.read_word("\"")
	if VM.compiling:
		(VM.compiling as WCForthWordCompiled).contents.push_back(st)
	else:
		VM.stack.push_back(st)
func _execi_stringname(VM:WCForth)->void:
	var st := VM.read_word("\"") as StringName
	if VM.compiling:
		(VM.compiling as WCForthWordCompiled).contents.push_back(st)
	else:
		VM.stack.push_back(st)
func _execi_nodepath(VM:WCForth)->void:
	var st := VM.read_word("\"") as NodePath
	if VM.compiling:
		(VM.compiling as WCForthWordCompiled).contents.push_back(st)
	else:
		VM.stack.push_back(st)
func _execi_bracket(VM:WCForth)->void:
	var code := VM.read_word("]")
	await VM.eval(code)
func _execi_comma(VM:WCForth)->void:
	if VM.compiling:
		var elem: Variant = VM.stack.pop_back()
		(VM.compiling as WCForthWordCompiled).contents.push_back(elem)

func _exec_plus(VM: WCForth)->void:
	var a: Variant = VM.stack.pop_back()
	var b: Variant = VM.stack.pop_back()
	VM.stack.push_back(b+a)
func _exec_minus(VM: WCForth)->void:
	var a: Variant = VM.stack.pop_back()
	var b: Variant = VM.stack.pop_back()
	VM.stack.push_back(b-a)
func _exec_multiply(VM: WCForth)->void:
	var a: Variant = VM.stack.pop_back()
	var b: Variant = VM.stack.pop_back()
	VM.stack.push_back(b*a)
func _exec_divide(VM: WCForth)->void:
	var a: Variant = VM.stack.pop_back()
	var b: Variant = VM.stack.pop_back()
	VM.stack.push_back(b/a)
func _exec_mod(VM: WCForth)->void:
	var a: Variant = VM.stack.pop_back()
	var b: Variant = VM.stack.pop_back()
	VM.stack.push_back(b%a)

func _exec_BREAK(VM:WCForth)->void:
	VM.jumps=1
	VM.breaks = 1+ VM.stack.pop_back()
func _exec_JUMP(VM:WCForth)->void:
	VM.breaks=1
	VM.jumps = 1+ VM.stack.pop_back()
func _exec_COND(VM:WCForth)->void:
	if not VM.stack.pop_back():
		VM.breaks = 1
		VM.jumps = 2

func _exec_eq(VM:WCForth)->void:
	var a: Variant = VM.stack.pop_back()
	var b: Variant = VM.stack.pop_back()
	VM.stack.push_back(a==b)
func _exec_lt(VM:WCForth)->void:
	var a: Variant = VM.stack.pop_back()
	var b: Variant = VM.stack.pop_back()
	VM.stack.push_back(a>b)

func _exec_AND(VM:WCForth)->void:
	var a: Variant = VM.stack.pop_back()
	var b: Variant = VM.stack.pop_back()
	if a is int and b is int:
		VM.stack.push_back(a & b)
	else:
		VM.stack.push_back(a and b)
func _exec_NOT(VM:WCForth)->void:
	var a: Variant = VM.stack.pop_back()
	if a is int:
		VM.stack.push_back(~a)
	else:
		VM.stack.push_back(!a)
func _exec_OR(VM:WCForth)->void:
	var a: Variant = VM.stack.pop_back()
	var b: Variant = VM.stack.pop_back()
	if a is int and b is int:
		VM.stack.push_back(a | b)
	else:
		VM.stack.push_back(a or b)

func _exec_rshift(VM:WCForth)->void:
	var a: Variant = VM.stack.pop_back()
	var b: Variant = VM.stack.pop_back()
	VM.stack.push_back(b >> a)

func _exec_lshift(VM:WCForth)->void:
	var a: Variant = VM.stack.pop_back()
	var b: Variant = VM.stack.pop_back()
	VM.stack.push_back(b << a)

func _exec_FALSE(VM:WCForth)->void: VM.stack.push_back(false)
func _exec_TRUE(VM:WCForth)->void: VM.stack.push_back(true)

func _exec_read(VM:WCForth)->void:
	if VM.stack.size()>1:
		var n: Variant = VM.stack.pop_back()
		var o: Variant = VM.stack.pop_back()
		if o is Object and (n is StringName or n is String):
			VM.stack.push_back((o as Object).get(n as StringName))
		elif o is Array or o is Dictionary:
			VM.stack.push_back(o[n])
func _exec_write(VM:WCForth)->void:
	if VM.stack.size()>2:
		var v: Variant = VM.stack.pop_back()
		var n: Variant = VM.stack.pop_back()
		var o: Variant = VM.stack.pop_back()
		if o is Object and (n is StringName or n is String):
			(o as Object).set(n as StringName,v)
		elif o is Array or o is Dictionary:
			o[n] = v

func _exec_call(VM:WCForth)->void:
	if VM.stack.size()>1 and VM.stack.back() is int:
		var num := VM.stack.pop_back() as int
		if VM.stack.size() < 1+num:
			return
		var ca: Variant = VM.stack.pop_back()
		if ca is Callable:
			var arr := []
			for i in range(num):
				arr.push_back(VM.stack.pop_back())
			var ret: Variant = (ca as Callable).callv(arr)
			if ret:
				VM.stack.push_back(ret)

func _exec_SWAP(VM:WCForth)->void:
	var a : Variant = VM.stack.pop_back()
	var b : Variant = VM.stack.pop_back()
	VM.stack.push_back(a)
	VM.stack.push_back(b)
func _exec_DROP(VM:WCForth)->void:
	VM.stack.pop_back()
func _exec_OVER(VM:WCForth)->void:
	var i := VM.stack.pop_back() as int
	VM.stack.push_back(VM.stack[-1-i])

func _execi_LIT(VM:WCForth)->void:
	var st := VM.read_word() as StringName
	if VM.dictionary.has(st):
		VM.stack.push_back(VM.dictionary[st])
