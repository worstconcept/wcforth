class_name WCForthWordCompiled
extends WCForthWord

@export var contents : Array

func exec(VM: WCForth)->void:
	var old_parent := VM.exec_parent
	VM.exec_parent = VM.executing
	VM.executing = self
	var idx := 0
	while idx < contents.size():
		var step : Variant = contents[idx]
		VM.breaks = 0
		VM.jumps = 1
		if step is WCForthWord:
			await (step as WCForthWord).exec(VM)
		else:
			VM.stack.push_back(step)
			if VM.builtin_await:
				await VM.builtin_await
		if VM.breaks > 0:
			VM.breaks-=1
			break
		idx += VM.jumps
	VM.executing = VM.exec_parent
	VM.exec_parent = old_parent


func _comp1(VM: WCForth, word: String)->void:
	if VM.dictionary.has(word):
		var w := VM.dictionary[word] as WCForthWord
		if w.immediate:
			await w.exec(VM)
		else:
			contents.push_back(w)
	elif word.is_valid_int():
		contents.push_back(int(word))
	elif word.is_valid_float():
		contents.push_back(float(word))

