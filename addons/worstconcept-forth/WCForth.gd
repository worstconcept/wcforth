@tool
class_name WCForth
extends Resource

@export var dictionary : Dictionary:
	set(val):
		dictionary = val
		if not val or val.is_empty():
			WCForthWordBuiltin.install(self, WCForthWordBuiltin)

var builtin_await : Signal

func _init() -> void:
	if Engine.is_editor_hint():
		if dictionary.is_empty():
			dictionary = {}


var stack := []
var src: String
var src_offs: int
var breaks: int
var jumps: int
var compiling: WCForthWord
var executing: WCForthWord
var exec_parent: WCForthWord

func eval(source: String)->void:
	var temp_offs := src_offs
	var temp_src := src
	var old_comp := compiling
	src_offs = 0
	src = source
	compiling = null
	await _eval_inplace()
	src_offs = temp_offs
	src = temp_src
	compiling = old_comp

func _eval_inplace()->void:
	while src_offs < src.length():
		var word := read_word()
		await _interp1(word)

func read_word(delim: String = " \t\n")->String:
	while src_offs < src.length() and delim.contains(src[src_offs]):
		src_offs += 1
	var from := src_offs
	while src_offs < src.length() and not delim.contains(src[src_offs]):
		src_offs += 1
	var to := src_offs - from
	if src_offs < src.length() and delim.contains(src[src_offs]):
		src_offs += 1
	return src.substr(from,to)

func _interp1(word: String)->void:
	if dictionary.has(word):
		await (dictionary[word] as WCForthWord).exec(self)
	elif word.is_valid_int():
		stack.push_back(int(word))
		if builtin_await:
			await builtin_await
	elif word.is_valid_float():
		stack.push_back(float(word))
		if builtin_await:
			await builtin_await
func compile(source: String)->WCForthWordCompiled:
	var temp_offs := src_offs
	var temp_src := src
	var old_comp := compiling
	src_offs = 0
	src = source
	var out := await _compile_inplace()
	src_offs = temp_offs
	src = temp_src
	compiling = old_comp
	return out

func _compile_inplace()->WCForthWordCompiled:
	var out := WCForthWordCompiled.new()
	var old_compiling := compiling
	compiling = out
	while src_offs < src.length():
		breaks = 0
		var word := read_word()
		await out._comp1(self,word)
		if breaks > 0:
			breaks -= 1
			break
	compiling = old_compiling
	return out
