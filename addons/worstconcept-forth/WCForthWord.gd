class_name WCForthWord
extends Resource

@export var immediate : bool

func exec(_VM: WCForth)->void:
	assert(await (Engine.get_main_loop() as SceneTree).process_frame, "executing base WCForthWord")
