extends Control

@export var forth : WCForth

@onready var dict: Label = %DICT
@onready var stk: Label = %STK
@onready var out: Label = %OUT
@onready var cmd: LineEdit = %CMD

var lock := false

class ugly_hack extends WCForthWord:
	var _out : Label
	func exec(VM: WCForth)->void:
		_out.text = str(VM.stack.pop_back()) + "\n" + _out.text

class uglier_hack extends WCForthWord:
	func exec(VM: WCForth)->void:
		VM.stack.push_back(Engine)

func _ready() -> void:
	forth.dictionary = forth.dictionary.duplicate(true)
	forth.dictionary["."] = ugly_hack.new()
	forth.dictionary["GD"] = uglier_hack.new()
	forth.dictionary["."]._out = out
	forth.builtin_await = ($Timer as Timer).timeout
	($Timer as Timer).timeout.connect(func()->void:
		await get_tree().process_frame
		restack()
	)
	cmd.grab_focus()
	restack()

func _on_line_edit_text_submitted(new_text: String) -> void:
	if lock:
		return
	lock = true
	cmd.editable = false
	if not new_text.is_empty():
		out.text = "⠇"+new_text+"\n" + out.text
		await forth.eval(new_text)
	restack()
	cmd.clear()
	cmd.editable = true
	lock = false

func restack()->void:
	stk.text = ""
	for i: Variant in forth.stack:
		stk.text = type_string(typeof(i)) + " | " + str(i).substr(0,20) + "\n" + stk.text
	stk.text = "STACK: \n" + stk.text
	dict.text = ""
	for i: StringName in forth.dictionary:
		#var j := forth.dictionary[i] as WCForthWord
		dict.text = i + "  " + dict.text
	dict.text = "WORDS: \n" + dict.text
